#include <stdio.h>
#include <stdlib.h>
#include <math.h>

float data_in(){

	int n,i;
	float a[100];
	printf("Digite o grau da equacao: ");
	scanf("%d",&n);
	printf("\n Entre com os coeficientes \n\n");

	for(i=n;i>=0;i--){
		printf("a[%d]: ",i);
		scanf("%f",&a[i]);
	}

	printf("\n\n O polinomio desejado eh ");

	//Tratar exibição do sinal (-), (não) exibição de coeficiente unitário  (a=1), 
	//casos onde o coeficiente é nulo, sinal final e não exibição de x^0. 
	for(i=n;i>=0;i--){

		if (i==n){
			if(a[i]>0){
				if(a[i]==1){
					printf("x^%d ",i);
				}else{
					printf("%.2fx^%d ",a[i],i);
				}
				i--;
			}else if(a[i]<0){
				if(a[i]==-1){
					printf("- x^%d ",i);
				}else{
					printf("- %.2fx^%d ",fabs(a[i]),i);
				}
				i--;
			}else if(a[i]==0){
				printf("\n Cuidado! A[%d] deve ser diferente de zero!",i);
				i = -2;
			}

		}

		if (i==1){
			if(a[i]>0){
				if(a[i]==1){
					printf("+ x ");
				}else{
					printf("+ %.2fx ",a[i]);
				}
				i--;
			}else if(a[i]<0){
				if(a[i]==-1){
					printf("- x ");
				}else{
					printf("- %.2fx ",fabs(a[i]));
				}
				i--;
			}else if(a[i]==0){
				i--;
			}

		}

		if(i==0){
			if(a[i]>0){
				printf("+ %.2f \n",a[i]);
			}
			if(a[i]<0){
				printf("- %.2f \n",fabs(a[i]));
			}
		}

		if(i>=2){
			if(a[i]<0){
				printf("- %.2fx^%d ",fabs(a[i]),i);
			}
			if(a[i]>0){
				printf("+ %.2fx^%d ",a[i],i);
			}
		}
	}
}

int main(){

	data_in();
	return 0;
}