#include <stdio.h>
#include <stdlib.h>
#include <math.h>

float fponto(float x, int n, float *a){

	float s,p;
	int i;
	s=0;
	p=0;
	for(i=0;i<=n;i++){
		p=pow(x,i);
		s=s+a[i]*p;
	}
	return(s);
}

float secantes(int n, float *a){

	float x0,x1,e;
	float f0,f1;
	float div,num,den;
	float x2=0;
	float x = 0;
	printf("\n Voce escolheu o metodo das secantes!\n");
	printf("\n Digite a precisao: e = ");
	scanf("%f",&e);
	printf("\n Digite a aproximação 1: X0 = ");
	scanf("%f",&x0);
	printf("\n Digite a aproximação 2: X1 = ");
	scanf("%f",&x1);

	do{
		x = x2;
		f0 = fponto(x0,n,a);
		f1 = fponto(x1,n,a);
		num = f1*(x1-x0);
		den = f1 - f0;
		div = num/den;
		x2 = x1 - div;

		printf("\n %f",x2);

		x0 = x1;
		x1 = x2;

	}while((fabs(x2-x)>e));


}

float data_in(){

	int n,i,op;
	float *a; //Ponteiro para vetor

	printf("\n Digite o grau do polinomio: ");
	scanf("%d",&n);
	printf("\n Entre com os coeficientes \n\n");

	a = (float *) malloc(n*sizeof(float)); //Alocação dinâmica de dados

	for(i=n;i>=0;i--){
		printf("a[%d]: ",i);
		scanf("%f",&a[i]);
	}

	secantes(n,a);
}

int main(){
	data_in();
}