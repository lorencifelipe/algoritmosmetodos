#include <stdio.h>
#include <stdlib.h>
#include <math.h>

//Função que retorna um vetor, sendo: b[n] = P(x0), b[n-1]...b[0] = Coeficientes de Q(x);
float *ruffini(int n, float *a, float p){
	int i;
	float *b;
	b = (float *) malloc(n*sizeof(float));

	for(i=(n-1);i>=0;i--){
		if(i==(n-1)){
			b[i]=a[n];
		}else{
			b[i]=a[i+1]+b[i+1]*p;
		}
	}

	b[n] = b[0]*p + a[0];

	return(b);
}

float horner(int n, float *a){

	int i;
	float x=0;
	float xn=0;
	float x0,e;
	float *b,*c,*d;
	float p,pl,div;
	b = (float *) malloc(n*sizeof(float));
	c = (float *) malloc((n-1)*sizeof(float));
	d = (float *) malloc((n-1)*sizeof(float));
	printf("\n Voce escolheu o metodo de Horner!");
	printf("\n Digite a aproximacao inicial: x0 = ");
	scanf("%f",&x0);
	printf("\n Digite a precisao: e = ");
	scanf("%f",&e);

	do{
		x = xn;
		b = ruffini(n,a,x0);
		p = b[n];

		for(i=(n-1);i>=0;i--){
			c[i] = b[i];
		}

		d = ruffini((n-1),c,x0);
		pl = d[n-1];
		div = (float)p/pl;
		xn = x0 - div;
		printf("\n xn = %f",xn);
		x0 = xn;
		
	}while((fabs(xn-x)>e));

	printf("\n ");

}

float data_in(){

	int n,i;
	float x0;
	float *a; //Ponteiro para vetor

	printf("\n Digite o grau do polinomio: ");
	scanf("%d",&n);
	printf("\n Entre com os coeficientes \n\n");

	a = (float *) malloc(n*sizeof(float)); //Alocação dinâmica de dados

	for(i=n;i>=0;i--){
		printf("a[%d]: ",i);
		scanf("%f",&a[i]);
	}

	horner(n,a);
}

int main(){
	data_in();
}