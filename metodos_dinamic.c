//Inclusão de bibliotecas 
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

float data_in(); //Protótipo de função data_in

//Função para impressão de mensagem de erro
void error(){
	printf("\n\n >> Cuidado: nao ha raiz no intervalo especificado!\n");
}

//Função para calcular o valor da função em um determinado ponto
float fponto(float x, int n, float *a){

	float s,p;
	int i;
	s=0;
	p=0;
	for(i=0;i<=n;i++){
		p=pow(x,i);
		s=s+a[i]*p;
	}
	return(s);
}

//Função para o Método da Bissecção 
float bissec(int n, int i, float *a){

	float e;
	float az,bz;
	float xa,xb;
	float xn,f1,f2;
	float x0 = 0;
	printf("\n\nVocê escolheu o método da bisseccao:\n");
	printf("\nDigite a precisao: ");
	scanf("%f",&e);
	printf("\nEntre com o intervalo [a,b]:\n");
	printf("a: ");
	scanf("%f",&az);
	printf("b: ");
	scanf("%f",&bz);

	xa = fponto(az,n,a);
	xb = fponto(bz,n,a);
	

	if(xa == 0){
		printf("\n\n >> A raiz da equacao eh exatamente: %f\n",az);
		exit(0);
	}

	if(xb == 0){
		printf("\n\n >> A raiz da equacao eh exatamente: %f\n",bz);
		exit(0);
	}

	if(xa*xb>0){
			error();
			exit(0);
		}

	xn = (az+bz)/2;


	while(fabs(x0-xn)>e)
	{
		x0 = xn;
		f1 = fponto(az,n,a);
		f2 = fponto(xn,n,a);

		if((f1*f2)>0){
			az = xn;
			xn = (az+bz)/2;
			printf("\n %f",xn);
		}else{
			bz = xn;
			xn = (az+bz)/2;
			printf("\n %f",xn);
		}
	
	}

	printf("\n A raiz aprox. da equacao, com precisao %f eh: %f\n",e,xn);

	exit(0);
}

//Função para calcular a derivada de uma função, retorna um vetor com os coeficientes correspondentes 
float *diff(int n, float *a){

	int i;
	float *b;
	b = (float *) malloc((n-1)*sizeof(float));
	

	for(i=0;i<=(n-1);i++){
		b[i]=a[i+1]*(i+1);
	}

	return(b);

}

//Função para o Método de Newton
float newton(int n, float *a){
	
	float e,xn,fx,fdx;
	float x = 0;
	float *b;
	float xnn;
	int i = 1;
	printf("\nVocê escolheu o metodo de Newton!\n");
	printf("\nDigite a precisao: e = ");
	scanf("%f",&e);
	printf("\nEntre com uma aproximacao inicial: X0 = ");
	scanf("%f",&xn);
	printf("\n");

	while(fabs(x-xn)>e){
		x = xn;
		fx=fponto(xn,n,a);
		b=diff(n,a);
		fdx=fponto(xn,(n-1),b);
		xnn = (float) fx/fdx;
		xn = xn - xnn; 
		printf("\n X%d = %f",i,xn);
		i++;
	}
	printf("\n");
	

}

//Função para o Método das Secantes
float secantes(int n, float *a){

	float x0,x1,e;
	float f0,f1;
	float div,num,den;
	float x2=0;
	float x = 0;
	printf("\n Voce escolheu o metodo das secantes!\n");
	printf("\n Digite a precisao: e = ");
	scanf("%f",&e);
	printf("\n Digite a aproximação 1: X0 = ");
	scanf("%f",&x0);
	printf("\n Digite a aproximação 2: X1 = ");
	scanf("%f",&x1);

	do{
		x = x2;
		f0 = fponto(x0,n,a);
		f1 = fponto(x1,n,a);
		num = f1*(x1-x0);
		den = f1 - f0;
		div = num/den;
		x2 = x1 - div;

		printf("\n %f",x2);

		x0 = x1;
		x1 = x2;

	}while((fabs(x2-x)>e));

	printf("\n\n");


}

//Função que retorna um vetor, sendo: b[n] = P(x0), b[n-1]...b[0] = Coeficientes de Q(x);
//Ruffini, para o Método de Horner
float *ruffini(int n, float *a, float p){
	int i;
	float *b;
	b = (float *) malloc(n*sizeof(float));

	for(i=(n-1);i>=0;i--){
		if(i==(n-1)){
			b[i]=a[n];
		}else{
			b[i]=a[i+1]+b[i+1]*p;
		}
	}

	b[n] = b[0]*p + a[0];

	return(b);
}

//Função para o Método de Horner
float horner(int n, float *a){

	int i;
	float x=0;
	float xn=0;
	float x0,e;
	float *b,*c,*d;
	float p,pl,div;
	b = (float *) malloc(n*sizeof(float));
	c = (float *) malloc((n-1)*sizeof(float));
	d = (float *) malloc((n-1)*sizeof(float));
	printf("\n Voce escolheu o metodo de Horner!");
	printf("\n Digite a aproximacao inicial: x0 = ");
	scanf("%f",&x0);
	printf("\n Digite a precisao: e = ");
	scanf("%f",&e);

	do{
		x = xn;
		b = ruffini(n,a,x0);
		p = b[n];

		for(i=(n-1);i>=0;i--){
			c[i] = b[i];
		}

		d = ruffini((n-1),c,x0);
		pl = d[n-1];
		div = (float)p/pl;
		xn = x0 - div;
		printf("\n xn = %f",xn);
		x0 = xn;
		
	}while((fabs(xn-x)>e));

	printf("\n ");

}

//Função para exibição da função digitada pelo usuário
void show_func(int n, int i, float *a){

	for(i=n;i>=0;i--){

		if (i==n){
			if(a[i]>0){
				if(a[i]==1){
					printf("x^%d ",i);
				}else{
					printf("%.2fx^%d ",a[i],i);
				}
				i--;
			}else if(a[i]<0){
				if(a[i]==-1){
					printf("- x^%d ",i);
				}else{
					printf("- %.2fx^%d ",fabs(a[i]),i);
				}
				i--;
			}else if(a[i]==0){
				printf("\n Cuidado! A[%d] deve ser diferente de zero!\n",i); //Chamar função de preenchimento novamente
				free(a);
				a = NULL;
				i = -2;
				data_in();
			}

		}

		if (i==1){
			if(a[i]>0){
				if(a[i]==1){
					printf("+ x ");
				}else{
					printf("+ %.2fx ",a[i]);
				}
				i--;
			}else if(a[i]<0){
				if(a[i]==-1){
					printf("- x ");
				}else{
					printf("- %.2fx ",fabs(a[i]));
				}
				i--;
			}else if(a[i]==0){
				i--;
			}

		}

		if(i==0){
			if(a[i]>0){
				printf("+ %.2f \n",a[i]);
			}
			if(a[i]<0){
				printf("- %.2f \n",fabs(a[i]));
			}
		}

		if(i>=2){
			if(a[i]<0){
				if(a[i]==-1){
					printf("- x^%d ",i);
				}else{
					printf("- %.2fx^%d ",fabs(a[i]),i);
				}
			}
			if(a[i]>0){
				if(a[i]==1){
					printf("+ x^%d ",i);
				}else{
					printf("+ %.2fx^%d ",a[i],i);
				}				
			}
		}
	}
}

//Função para entrada de dados: o usuário digita o grau e os coeficientes da função
float data_in(){

	int n,i,op;
	float *a; //Ponteiro para vetor

	printf("\n Digite o grau do polinomio: ");
	scanf("%d",&n);
	printf("\n Entre com os coeficientes \n\n");

	a = (float *) malloc(n*sizeof(float)); //Alocação dinâmica de dados

	for(i=n;i>=0;i--){
		printf("a[%d]: ",i);
		scanf("%f",&a[i]);
	}

	printf("\nA funcao a ser analisada eh: \n\n f(x) = ");

	show_func(n,i,a);

	//Menu: o usuário escolhe o método para encontrar a raiz da função

	printf("\n\nDigite o numero correspondente ao metodo desejado:\n");
	printf("\n [1] Bisseccao;");
	printf("\n [2] Newton;");
	printf("\n [3] Secantes;");
	printf("\n [4] Horner;");
	printf("\n [5] Desejo reescrever minha função");
	printf("\n => ");
	scanf("%d",&op);

	switch(op){
		case 1:
		bissec(n,i,a);
		break;

		case 2:
		newton(n,a);
		break;

		case 3:
		secantes(n,a);
		break;

		case 4:
		horner(n,a);
		break;

		case 5:
		free(a);
		a = NULL;
		data_in();
		break;

		default:
		printf("\n Valor inválido! \n");
		free(a);
		a = NULL;
		data_in();
	}
	

}

//Função principal
int main(){

	data_in(); //Chamada para função de entrada de dados
	return 0;
}