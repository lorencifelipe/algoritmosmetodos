#include <math.h>
#include <stdlib.h>
#include <stdio.h>


float *diff(int n, float *a){

	int i;
	float *b;
	b = (float *) malloc((n-1)*sizeof(float));
	

	for(i=0;i<=(n-1);i++){
		b[i]=a[i+1]*(i+1);
	}

	return(b);

}

float data_in(){

	int n,i,op;
	float *a,*b; //Ponteiro para vetor

	printf("\n Digite o grau do polinomio: ");
	scanf("%d",&n);
	printf("\n Entre com os coeficientes \n\n");

	a = (float *) malloc(n*sizeof(float)); //Alocação dinâmica de dados

	for(i=n;i>=0;i--){
		printf("a[%d]: ",i);
		scanf("%f",&a[i]);
	}

//Teste de retorno de função

	b = diff(n,a);

	for(i=(n-1);i>=0;i--){
		printf("\n b[%d] = %f",i,b[i]);
	}
}

int main(){
	
	data_in();

	
}