#include <math.h>
#include <stdlib.h>
#include <stdio.h>

float fponto(float x, int n, float *a){

	float s,p;
	int i;
	s=0;
	p=0;
	for(i=0;i<=n;i++){
		p=pow(x,i);
		s=s+a[i]*p;
	}
	return(s);
}

float *diff(int n, float *a){

	int i;
	float *b;
	b = (float *) malloc((n-1)*sizeof(float));
	

	for(i=0;i<=(n-1);i++){
		b[i]=a[i+1]*(i+1);
	}

	return(b);

}

float newton(int n, float *a){

	//Implementar função de verificação de convergência
	
	float e,xn,fx,fdx;
	float x = 0;
	float *b;
	float xnn;
	int i = 1;
	printf("\nVocê escolheu o metodo de Newton!\n");
	printf("\nDigite a precisao: e = ");
	scanf("%f",&e);
	printf("\nEntre com uma aproximacao inicial: X0 = ");
	scanf("%f",&xn);
	printf("\n");

	while(fabs(x-xn)>e){
		x = xn;
		fx=fponto(xn,n,a);
		b=diff(n,a);
		fdx=fponto(xn,(n-1),b);
		xnn = (float) fx/fdx;
		xn = xn - xnn; 
		printf("\n x%d = %f",i,xn);
		i++;
	}
	printf("\n");
	

}

float data_in(){
 
	int n,i,op;
	float *a,*b; //Ponteiro para vetor

	printf("\n Digite o grau do polinomio: ");
	scanf("%d",&n);
	printf("\n Entre com os coeficientes \n\n");

	a = (float *) malloc(n*sizeof(float)); //Alocação dinâmica de dados

	for(i=n;i>=0;i--){
		printf("a[%d]: ",i);
		scanf("%f",&a[i]);
	}

	newton(n,a);
}

int main(){
	data_in();
}